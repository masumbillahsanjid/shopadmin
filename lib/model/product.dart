import 'package:flutter/foundation.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final List colors;
  final String Category;
  final double price;
  final String ImageUrl;


  Product(
      {@required this.id,
      @required this.title,
      @required this.description,
      @required this.colors,
      @required this.Category,
      @required this.price,
      @required this.ImageUrl});


}

import 'package:adminshop/model/product.dart';
import 'package:adminshop/provider/providerProduct.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class EditProduct extends StatefulWidget {
  static const routeName = "/edit-product";

  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  final _priceFocusNode = FocusNode();
  final _desCriptionNOde = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _editedproduct = Product(
      id: null,
      title: "",
      colors: [],
      description: "",
      Category: "",
      price: 0,
      ImageUrl: "");

  var _isInit = true;
  var _initValues = {
    "title": "",
    "price": "",
    "colors": ["black", "green"],
    'Category': "",
    "description": "",
    'ImageUrl': ""
  };

  @override
  void dispose() {
    // TODO: implement dispose
    _imageFocusNode.removeListener(_upadteImageUrl);
    _desCriptionNOde.dispose();
    _priceFocusNode.dispose();
    _imageUrlController.dispose();
    _imageFocusNode.dispose();

    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement
    //  initState

    _imageFocusNode.addListener(_upadteImageUrl);
    print(_editedproduct.colors.runtimeType);

    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {

        _editedproduct =
            Provider.of<providerProduct>(context).productByid(productId);
        _initValues = {
          "title": _editedproduct.title,
          "price": _editedproduct.price.toString(),
          "colors": _editedproduct.colors,
          "Category": _editedproduct.Category,
          'description': _editedproduct.description,
          "ImageUrl": "",
        };
        _imageUrlController.text = _editedproduct.ImageUrl;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  var _isloading = false;

  void _upadteImageUrl() {
    if (!_imageFocusNode.hasFocus) {
      setState(() {});
    }
  }

  void _saveForm() async {
    final isvalid = _form.currentState.validate();
    if (!isvalid) {
      return;
    }
    setState(() {
      _isloading = true;
    });
    _form.currentState.save();
    if (_editedproduct.id != null) {
      await Provider.of<providerProduct>(context, listen: false)
          .updateProduct(_editedproduct.id, _editedproduct);
    } else {
      _editedproduct.colors.add("green");
      await Provider.of<providerProduct>(context, listen: false)
          .addproduct(context,_editedproduct)
          .catchError((error) {
        return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text("An error occures!"),
                  content: Text("Something Went wrong"),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.pop(ctx);
                        },
                        child: Text("Okay"))
                  ],
                ));
      }).then((values) {
        setState(() {
          _isloading = false;
        });
        Navigator.of(context).pop();
      });
    }
    setState(() {
      _isloading = false;
    });
//    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Product"),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.save), onPressed: _saveForm)
          ],
        ),
        body: _isloading
            ? Center(child: CircularProgressIndicator())
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                    key: _form,
                    child: ListView(
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(labelText: "Title"),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (value) {
                            FocusScope.of(context)
                                .requestFocus(_priceFocusNode);
                          },
                          initialValue: _initValues['title'],
                          onSaved: (value) {
                            _editedproduct = Product(
                              colors: _editedproduct.colors,
                              title: value,
                              Category: _editedproduct.Category,
                              price: _editedproduct.price,
                              description: _editedproduct.description,
                              ImageUrl: _editedproduct.ImageUrl,
                              id: _editedproduct.id,
                            );
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please provide a value";
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: "Price"),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          focusNode: _priceFocusNode,
                          initialValue: _initValues['price'],
                          onFieldSubmitted: (value) {
                            FocusScope.of(context)
                                .requestFocus(_desCriptionNOde);
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please provide a value";
                            }
                            if (double.tryParse(value) == null) {
                              return "Please enter a valid number";
                            }
                            if (double.parse(value) <= 0) {
                              return "please enter a number";
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _editedproduct = Product(
                                id: _editedproduct.id,
                                colors: _editedproduct.colors,
                                Category: _editedproduct.Category,
                                title: _editedproduct.title,
                                price: double.parse(value),
                                description: _editedproduct.description,
                                ImageUrl: _editedproduct.ImageUrl);
                          },
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: "Category"),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (value) {
                            FocusScope.of(context)
                                .requestFocus(_priceFocusNode);
                          },
                          initialValue: _initValues['Category'],
                          onSaved: (value) {
                            _editedproduct = Product(
                              title: _editedproduct.title,
                              colors: _editedproduct.colors,
                              Category: value,
                              price: _editedproduct.price,
                              description: _editedproduct.description,
                              ImageUrl: _editedproduct.ImageUrl,
                              id: _editedproduct.id,
                            );
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please provide a value";
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: "DesCiption"),
                          maxLines: 3,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.multiline,
                          focusNode: _desCriptionNOde,
                          initialValue: _initValues['description'],
                          validator: (value) {
                            if (value.isEmpty) {
                              return "please provide a value";
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _editedproduct = Product(
                                id: _editedproduct.id,
                                colors: _editedproduct.colors,
                                Category: _editedproduct.Category,
                                title: _editedproduct.title,
                                price: _editedproduct.price,
                                description: value,
                                ImageUrl: _editedproduct.ImageUrl);
                          },
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                width: 100,
                                height: 100,
                                margin: EdgeInsets.only(top: 8, right: 10),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey)),
                                child: _imageUrlController.text.isEmpty
                                    ? Text("Enter Url")
                                    : FittedBox(
                                        child: Image.network(
                                            _imageUrlController.text),
                                        fit: BoxFit.cover,
                                      )),
                            Expanded(
                              child: TextFormField(
                                decoration:
                                    InputDecoration(labelText: "Image Url"),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "please provide a value";
                                  }
                                  return null;
                                },
                                keyboardType: TextInputType.url,
                                textInputAction: TextInputAction.done,
                                controller: _imageUrlController,
                                focusNode: _imageFocusNode,
                                onSaved: (value) {
                                  _editedproduct = Product(
                                      id: _editedproduct.id,
                                      colors: _editedproduct.colors,
                                      title: _editedproduct.title,
                                      Category: _editedproduct.Category,
                                      price: _editedproduct.price,
                                      description: _editedproduct.description,
                                      ImageUrl: value);
                                },
                                onFieldSubmitted: (value) {
                                  _saveForm();
                                },
                              ),
                            )
                          ],
                        )
                      ],
                    )),
              ));
  }
}

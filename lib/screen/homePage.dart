import 'package:adminshop/provider/auth.dart';
import 'package:adminshop/provider/order.dart';
import 'package:adminshop/screen/editandadd.dart';
import 'package:adminshop/screen/signIn.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 100), () {
      Provider.of<Order>(context, listen: false).fetchandretirve(context);
    });

    fetchandretirve();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final myorder = Provider.of<Order>(context, listen: true).items;
    final order = Provider.of<Order>(context, listen: false);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        actionsIconTheme: IconThemeData(
          color: Theme.of(context).primaryColor
        ),
        actions: <Widget>[
          FlatButton(
              child: Text(
                "Log Out",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                SignOut();
              })
        ],
        title: Text("admin"),
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "User Name",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                "Image",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                "Quantity",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                "Status",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          myorder.isEmpty
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: myorder.length,
                  itemBuilder: (_, index) {
                    return Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(myorder[index].username),
                            Image.network(
                              myorder[index].image,
                              height: 40,
                              width: 40,
                            ),
                            Text(myorder[index].quantity.toString()),
                            Text(myorder[index].status == false
                                ? "pending"
                                : "approved"),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RaisedButton(
                              color: Colors.green[800],
                              onPressed: () {
                                if (myorder[index].status == false) {
                                  DisPlaySnack(
                                      context, 'Product Approved Sccessfully');
                                  order.updateProduct(
                                      myorder[index].id, true, index);
                                  Future.delayed(Duration(milliseconds: 500),
                                      () {
                                    Provider.of<Order>(context, listen: false)
                                        .fetchandretirve(context);
                                  });
                                } else {
                                  DisPlaySnack(
                                      context, 'Already this is approved');
                                }
                              },
                              child: Text(
                                "Approved",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            RaisedButton(
                              color: Colors.red[800],
                              onPressed: () {
                                if (myorder[index].status == false) {
                                  DisPlaySnack(
                                      context, "This is already pending");
                                } else {
                                  DisPlaySnack(
                                      context, "Product cancel successfully");
                                  order.updateProduct(
                                      myorder[index].id, false, index);
                                  Future.delayed(Duration(milliseconds: 500),
                                      () {
                                    Provider.of<Order>(context, listen: false)
                                        .fetchandretirve(context);
                                  });
                                }
                              },
                              child: Text("Cancel",
                                  style: TextStyle(color: Colors.white)),
                            )
                          ],
                        ),
                        Divider(),
                      ],
                    );
                  })
        ],
      ),
    );
  }

  Future<void> fetchandretirve() async {
    var url =
        "https://myshop-853e9.firebaseio.com/order.json?auth=${Provider.of<Auth>(context, listen: false).getToken}";

    var response = await http.get(url);

    if (response.statusCode == 200) {
      print(response.body);
    } else {}
  }

  Future SignOut() async {
    SharedPreferences getSp = await SharedPreferences.getInstance();

    Provider.of<Order>(context, listen: false).clear();
    await getSp.clear();
    Route route = MaterialPageRoute(builder: (context) => Login());
    Navigator.push(context, route);
  }

  DisPlaySnack(BuildContext context, String mytext) {
    _scaffoldKey.currentState.hideCurrentSnackBar();
    final snackBar = SnackBar(content: Text(mytext));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

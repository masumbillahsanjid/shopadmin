import 'package:adminshop/screen/homePage.dart';
import 'package:adminshop/provider/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  Color maincolor = Color.fromRGBO(1, 117, 216, 1.0);
  String _email, _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _isloading = false;
  final _scaffoldsi = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldsi,
        backgroundColor: Color.fromRGBO(250, 251, 255, 1.0),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
              ),
              Center(
                  child: Text(
                "Welcome To",
                style: TextStyle(fontSize: 20),
              )),
              Text(
                "Admin Panel",
                style: TextStyle(
                    color: maincolor,
                    fontSize: 40,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "This is a online gaming site for you",
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.87,
                        child: TextFormField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person_outline),
                              labelText: 'email',
                              border: OutlineInputBorder()),
                          onSaved: (input) => _email = input,
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.87,
                        child: TextFormField(
                          obscureText: true,
                          onSaved: (input) => _password = input,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person_outline),
                              labelText: 'Password',
                              border: OutlineInputBorder()),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      _isloading
                          ? Center(
                              child: CircularProgressIndicator(),
                            )
                          : Container(
                              width: MediaQuery.of(context).size.width * 0.87,
                              height: MediaQuery.of(context).size.width * 0.12,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        color: maincolor.withOpacity(0.4),
                                        offset: Offset(0, 0),
                                        blurRadius: 5,
                                        spreadRadius: 2)
                                  ]),
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                color: maincolor,
                                onPressed: () {
                                  _formKey.currentState.save();

                                  if (_email == "admin@test.com") {
                                    signIn().then((value) {

                                      _isloading = false;
                                    });
                                  } else {
                                    DisPlaySnack(
                                        context, "Enter Admin Account");
                                  }
                                },
                                child: Text(
                                  "Login",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      FlatButton(
                        onPressed: () {
//                          Route route =
//                          MaterialPageRoute(builder: (context) => Signup());
//                          Navigator.push(context, route);
                        },
                        child: Text(
                          "Create Account",
                          style: TextStyle(
                              color: maincolor, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ));
  }

  Future<void> signIn() async {
    setState(() {
      _isloading = true;
    });

    await Provider.of<Auth>(context, listen: false)
        .SignIn(context, _email, _password)
        .catchError((value) {
      setState(() {
        _isloading = false;
      });
    });
  }

  DisPlaySnack(BuildContext context, String mytext) {
    _scaffoldsi.currentState.hideCurrentSnackBar();
    final snackBar = SnackBar(content: Text(mytext));
    _scaffoldsi.currentState.showSnackBar(snackBar);
  }
}

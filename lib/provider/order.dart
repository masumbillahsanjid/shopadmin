import 'package:adminshop/provider/auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class OrderItem {
  final String id;
  final String username;
  final String image;
  final int quantity;
  final bool status;
  final double totalamount;

  OrderItem({
    @required this.id,
    @required this.username,
    @required this.image,
    @required this.status,
    @required this.quantity,
    @required this.totalamount,
  });
}

class Order with ChangeNotifier {
  List<OrderItem> _items = [];

  List<OrderItem> get items {
    return [..._items];
  }

  Future<void> fetchandretirve(BuildContext context) async {
    SharedPreferences getSp = await SharedPreferences.getInstance();

    var url =
        "https://myshop-853e9.firebaseio.com/order.json?auth=${getSp.get("idToken")}";

    var response = await http.get(url);

    if (response.statusCode == 200) {
      final extractData = json.decode(response.body) as Map<String, dynamic>;
      final List<OrderItem> loadedProduct = [];
      extractData.forEach((proid, prodData) {
        loadedProduct.add(OrderItem(
          id: proid,
          username: prodData['username'],
          image: prodData['image'],
          status: prodData['status'],
          totalamount: double.parse(prodData['totalamount']),
          quantity: int.parse(prodData['quantity']),
        ));

        _items = loadedProduct;

        notifyListeners();
      });
    } else {}
  }

  Future<void> updateProduct(String id, bool data, int index) async {
    SharedPreferences getSp = await SharedPreferences.getInstance();
    final prodIndex = _items.indexWhere((prod) => prod.id == id);

    var url =
        "https://myshop-853e9.firebaseio.com/order/$id.json?auth=${getSp.get("idToken")}";

    var response = await http.patch(url, body: json.encode({"status": data}));

    if (response.statusCode == 200) {}
  }

  void clear() {
    _items = [];
    notifyListeners();
  }
}

import 'package:adminshop/model/product.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:provider/provider.dart';

import 'auth.dart';

class providerProduct with ChangeNotifier {
  List<Product> _item = [];

  List<Product> get item {
    return [..._item];
  }

  Future<void> addproduct(BuildContext context, Product product) {

    var url = "https://myshop-853e9.firebaseio.com/products.json?auth=${Provider.of<Auth>(context,listen: false).getToken}";

    return http
        .post(url,
            body: json.encode({
              "title": product.title,
              "colors" : product.colors,
              "description": product.description,
              "Category": product.Category,
              "price": product.price,
              "ImageUrl": product.ImageUrl,
            }))
        .then((response) {
      final newProduct = Product(
          id: json.decode(response.body)['name'],
          title: product.title,
          colors: product.colors,
          Category: product.Category,
          description: product.description,
          price: product.price,
          ImageUrl: product.ImageUrl);
      _item.add(newProduct);

      notifyListeners();
    }).catchError((error) {
      print(error);
      throw error;
    });
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final prodIndex = _item.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      final url = "https://myshop-853e9.firebaseio.com/products/$id.json";
      await http.patch(url,
          body: json.encode({
            "title": newProduct.title,
            "colors" : newProduct.colors,
            "description": newProduct.description,
            "Category": newProduct.Category,
            "price": newProduct.price,
            "ImageUrl": newProduct.ImageUrl,
          }));
      _item[prodIndex] = newProduct;
      notifyListeners();
    } else {
      print("_____");
    }
  }

  Product productByid(String id) {
    return _item.firstWhere((data) => data.id == id);
  }
}

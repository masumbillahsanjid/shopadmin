import 'package:adminshop/screen/signIn.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../screen/homePage.dart';

class Auth with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  String _userdId;
  String message;

  set setToken(value) {
    _token = value;
  }

  get getToken {
    return _token;
  }

  Future<void> SignIn(
      BuildContext context, String email, String password) async {
    SharedPreferences setSp = await SharedPreferences.getInstance();
    const url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBxAIMXMad2wlS-ZKxuzskKQCEc41T751w";

    final response = await http
        .post(url,
        body: json.encode({
          "email": email,
          "password": password,
          "returnSecureToken": true
        }))
        .catchError((error) {
      print(error);
    });

    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      setSp.setString("idToken", data['idToken']);
      setToken = data['idToken'];
      print(data);
      Route route = MaterialPageRoute(builder: (context) => HomePage());
      Navigator.push(context, route);
      notifyListeners();
    } else {
      print(response.body);
    }
  }
}

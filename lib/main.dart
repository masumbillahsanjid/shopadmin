import 'package:adminshop/provider/auth.dart';
import 'package:adminshop/provider/order.dart';
import 'package:adminshop/provider/providerProduct.dart';
import 'package:adminshop/screen/editandadd.dart';
import 'package:adminshop/screen/signIn.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'screen/homePage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences getSP = await SharedPreferences.getInstance();
  var email = getSP.getString('idToken');
  print(email);
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Auth()),
        ChangeNotifierProvider.value(value: providerProduct()),
        ChangeNotifierProvider.value(value: Order()),
      ],
      child: MaterialApp(
          theme: ThemeData(
            primarySwatch: Colors.purple, accentColor: Colors.deepOrange),
          debugShowCheckedModeBanner: false,
          home: email == null ? Login() : HomePage())));
}


